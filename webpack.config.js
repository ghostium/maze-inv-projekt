var webpack = require('webpack');

var wpconfig = {
  entry: ['babel-polyfill', './src/index.html', './src/app.js'],
  output: {
    path: 'dist/',
    filename: 'app.js'
  },
  target: 'web',
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'stage-3']
        }
      },
      {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]'
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  externals: {},
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin()
  ]
};

module.exports = wpconfig;

import {Map} from 'immutable'
import flow from 'lodash.flow'
import curry from 'lodash.curry'
import renderer from './js/maze/view/renderer'
import {createEmptyMaze, createOuterBoarders, chooseStart, replaceStartWithPlayer} from './js/maze/impl/creator'
import {generateMaze} from './js/maze/impl/generator'
import {interactiveMaze} from './js/maze/view/gamehandler'

let canvas = document.getElementById("maze");

const color = Map({
  WALL: "#000000",
  AIR: "#FFFFFF",
  START: "#00FF00",
  END: "#FF0000",
  PLAYER: "#8e44ad"
});
const basesize = 50;

const renderEntitiesToCanvas = curry(renderer)(canvas, 8, color);
let play = flow(createEmptyMaze.bind(null, basesize, basesize),
  curry(createOuterBoarders)(basesize, basesize),
  chooseStart,
  generateMaze,
  replaceStartWithPlayer,
  (mazeMap) => {
    renderEntitiesToCanvas(mazeMap);
    return mazeMap;
  },
  curry(interactiveMaze)(renderEntitiesToCanvas));

document.getElementById("play").addEventListener('click', play);

export default play;

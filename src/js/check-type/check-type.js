/**
 * Functions for fail-fast type and schema safety.
 * Lazy-assert can be easily disabled in production and the function call causes almost no overhead. <br />
 * <br />
 * There are code duplicates because different error messages are printed out. Passing them as an argument would waste the original idea of lazy
 * assert. Read more here: <a href="https://github.com/bahmutov/lazy-ass#example">Lazy-ass Github</a>
 * @module checktype
 */
import lazyAss from 'lazy-ass'
import {List, Map} from 'immutable'
import {is as isType, of as typeAsString} from 'type-of-is'

/**
 * Check if value is type. Compares constructor functions or if the object hasn't a constructor it compares it with the
 * Object.toString() output.
 * @param {*} value Javascript value
 * @param {Object} type Standard built-in object
 * @throws {Error} Value isn't type
 */
export function checkType(value, type) {
  lazyAss(() => isType(value, type), "Wrong type. Wanted: ", () => typeAsString(type), " Given: ", () => typeAsString(type));
}

/**
 * Check every value in the given immutable.js list is type. Compares constructor functions or if the object hasn't a constructor it compares it with the
 * Object.toString() output.
 * @param {Immutable.List} collection Immutable.js list object
 * @param {Object} type Standard built-in object
 * @throws {Error} Every value isn't type in immutable.js list
 * @throws {Error} Argument collection isn't an immutable.js list object
 */
export function checkTypeInList(collection, type) {
  lazyAss(() => collection.every((element) => isType(element, type)), "One value in immutable.js list isn't the type wanted. Wanted: ",
    () => typeAsString(type), " Given: ", () => typeAsString(type));
}

/**
 * Validate if value pass typeValidationFunction.
 * @param {*} value A javascript value
 * @param {Function} typeValidationFunction The function called for validation
 * @throws {Error} Validation function returns false for value
 */
export function validateType(value, typeValidationFunction) {
  lazyAss(() => typeValidationFunction(value), "Value isn't valid in validation function.  Value type: ",
    () => typeAsString(value), " Function: ", () => typeAsString(typeValidationFunction));
}

/**
 * Check every value in the given immutable.js list pass validation function.
 * @param {Immutable.List} collection Immutable.js list object
 * @param {Function} typeValidationFunction The function called for validation
 * @throws {Error} Every value in immutable.js list doesn't pass the validation function
 * @throws {Error} Argument collection isn't an immutable.js list object
 */
export function validateTypeInList(collection, typeValidationFunction) {
  lazyAss(() => List.isList(collection), "Unexpected type of argument collection. Wanted an immutable.js list, got ", () => typeAsString(collection));
  lazyAss(() => collection.every((element) => typeValidationFunction(element)), "One or more values inside the list doesn't match the required type.");
}

/**
 * Check if map passes schema validation function.
 * @param {Immutable.Map} map The map that will be checked
 * @param {Function} mapSchemaValidationFunction The function that will be used to check the schema
 * @throws {Error} Schema validation function returns false for map
 * @throws {Error} Argument map isn't an immutable.js map object
 */
export function validateMapSchema(map, mapSchemaValidationFunction) {
  lazyAss(() => Map.isMap(map), "Unexpected type of argument map. Wanted an immutable.js map, got ", () => typeAsString(map));
  lazyAss(() => mapSchemaValidationFunction(map.toObject()), "Map doesn't match schema.");
}

/**
 * Check if every map value in list map passes schema validation function.
 * @param {Immutable.List<Map>} collection Immutable.js list object
 * @param  {Function} mapSchemaValidationFunction The function that will be used to check the schema
 * @throws {Error} Schema validation function returns false for one map in list
 * @throws {Error} Argument collection isn't an immutable.js list object
 */
export function validateMapSchemaInList(collection, mapSchemaValidationFunction) {
  lazyAss(() => List.isList(collection), "Unexpected type of argument collection. Wanted an immutable.js list, got ", () => typeAsString(collection));
  lazyAss(() => collection.every((map) => Map.isMap(map) && mapSchemaValidationFunction(map.toObject())), "One or more maps inside the list doesn't match schema.");
}


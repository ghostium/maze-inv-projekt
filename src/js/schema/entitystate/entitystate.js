/**
 * Representation and validation functions for the type EntityState
 * @module schema/EntityState
 */
import Ajv from 'ajv'
import EntityStateSchema from './entitystate.schema.json'

const ajv = new Ajv();
/**
 * Check if object is a valid EntityState
 * @function
 * @static
 * @param {Object} data The object that should be checked
 * @return {boolean} If the object matches
 * @see entitystate.schema.json
 */
const validateStateSchema = ajv.compile(EntityStateSchema);

/**
 * A string that represents the internal state of an entity
 * @typedef {string} EntityState
 */
const EntityState = EntityStateSchema["enum"].reduce((enumObject, enumString) => {
  enumObject[enumString] = enumString;
  return enumObject;
}, Object.create(null));
//Enum object should not be editable
Object.freeze(EntityState);

export {validateStateSchema, EntityState};

/**
 * Representation and validation functions for the type Entity
 * @module schema/Entity
 */
import Ajv from 'ajv'
import Immutable from 'immutable'
import EntitySchema from './entity.schema.json'
import EntityStateSchema from '../entitystate/entitystate.schema.json'
import {checkType} from '../../check-type/check-type'


let ajv = new Ajv();
ajv.addSchema(EntityStateSchema);

/**
 * Check if object is a valid Entity
 * @function
 * @static
 * @param {Object} data The object that should be checked
 * @return {boolean} If the object matches
 * @see entity.schema.json
 */
const validateSchema = ajv.compile(EntitySchema);

/**
 * Entity factory
 * @param x x position
 * @param y y position
 * @param state The state the entity should be
 * @static
 * @returns {Immutable.Map} the entity
 */
function createEntity(x, y, state = "AIR") {
  checkType(x, Number);
  checkType(y, Number);
  return Immutable.Map({x, y, state})
}

export {createEntity, validateSchema as isAnEntity};

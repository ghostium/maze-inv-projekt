/**
 * Functions that handle game logic and events
 * @module maze/view/gamehandler
 */

import curry from 'lodash.curry'
import {validateMapSchemaInList, checkType} from '../../check-type/check-type'
import {isAnEntity} from '../../schema/entity/entity'
import {EntityState} from '../../schema/entitystate/entitystate'
import * as EntityFilter from '../entity/filter'
import {List} from 'immutable'

const eventname = "keydown";

let handlerState = {
  renderFunction() {},
  mazeMap: List(),
  registered: false,
  finished: false
};

/**
 * Handle keyevents and apply gamelogic to the maze.
 * When the player reached the end of the maze, keys will no longer applied to the maze map.
 * @param {KeyboardEvent} event The keydown event that happened when the player pressed a key
 */
function eventHandler(event) {
  if (!handlerState.finished) {
    handlerState.mazeMap = applyKeyEvent(handlerState.mazeMap, event);
    handlerState.renderFunction(handlerState.mazeMap);
    //Reached end?
    if (handlerState.mazeMap.filter(curry(EntityFilter.state)(EntityState.END)).isEmpty()) {
        handlerState.finished = true;
        alert("You have won!")
    }
  }
}


/**
 * Move the player to wanted position (x,y)
 * @param mazeMap the maze entity list that should be mutated.
 * @param x x-axis target position
 * @param y y-axis target position
 * @returns {Immutable.List<Immutable.Map>} the maze where the player moved
 */
function applyWalkIntention(mazeMap, x, y) {
  checkType(x, Number);
  checkType(y, Number);
  validateMapSchemaInList(mazeMap, isAnEntity);
  let playersInMaze = mazeMap.filter(curry(EntityFilter.state)(EntityState.PLAYER));
  //Only move when there is exact one player in maze
  if (!(playersInMaze.isEmpty() || playersInMaze.size > 1)) {
    let player = playersInMaze.first();
    let playerIndex = mazeMap.indexOf(player);
    let moveGoalEntityList = mazeMap.filter(curry(EntityFilter.position)(player.get("x") + x, player.get("y") + y));
    //Out of bounds?
    if (!moveGoalEntityList.isEmpty()) {
      let desiredMoveGoal = moveGoalEntityList.first();
      let desiredGoalIndex = mazeMap.indexOf(desiredMoveGoal);
      //You can't walk against walls
      if (!(desiredMoveGoal.get("state") === EntityState.WALL)) {
        //Set position to start if player state overlapped the start state
        player = player.set("state", (mazeMap.filter(curry(EntityFilter.state)(EntityState.START)).isEmpty()) ? EntityState.START : EntityState.AIR);
        desiredMoveGoal = desiredMoveGoal.set("state", EntityState.PLAYER);
        return mazeMap.set(desiredGoalIndex, desiredMoveGoal).set(playerIndex, player);
      }
    }
  }
  return mazeMap;
}

/**
 * Move the player to a position in the maze, when the specific key was pressed.
 * W - Up, D - Left, S - Down, A - Right
 * @param {Immutable.List<Immutable.Map>} mazeMap Maze map where the event should me applied
 * @param {KeyboardEvent} event The keydown event that happened when the player pressed a key
 * @returns {Immutable.List<Immutable.Map>} the maze where the player moved
 */
function applyKeyEvent(mazeMap, event) {
  validateMapSchemaInList(mazeMap, isAnEntity);
  checkType(event, KeyboardEvent);
  const walk = curry(applyWalkIntention)(mazeMap);
  switch (event.key) {
    case "s":
      return walk(0, 1);
    case "w":
      return walk(0, -1);
    case "a":
      return walk(-1, 0);
    case "d":
      return walk(1, 0);
    default:
      return mazeMap;
  }
}

/**
 * Reset the game state
 */
function resetGameHandlerState() {
  //Remove old listener if registered
  if (handlerState.registered) {
    window.removeEventListener(eventname, eventHandler);
  }
  //Reset handler state
  handlerState = {
    renderFunction() {},
    mazeMap: List(),
    registered: false,
    finished: false
  };
}

/**
 * Enable event handling and game logic. Overwrites any old handler state
 * @param renderFunction the function that gets mutated maze versions to render
 * @param mazeMap initial generated maze
 * @static
 */
function interactiveMaze(renderFunction, mazeMap) {
  checkType(renderFunction, Function);
  validateMapSchemaInList(mazeMap, isAnEntity);
  resetGameHandlerState();
  handlerState.mazeMap = mazeMap;
  handlerState.renderFunction = renderFunction;
  window.addEventListener('keydown', eventHandler);
  handlerState.registered = true;
}


export {interactiveMaze};

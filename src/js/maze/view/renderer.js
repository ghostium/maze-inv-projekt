/**
 * The render functions that use a maze list and render it on a canvas
 * @module maze/view/renderer
 */

import {isAnEntity} from '../../schema/entity/entity'
import {checkType, validateMapSchema, validateMapSchemaInList} from '../../check-type/check-type'
import curry from 'lodash.curry'

/**
 * Draws entities with settings on a canvas. Clears the old image before.
 * @param {HTMLCanvasElement} canvas The target canvas, which will be cleared, and the state then will be painted on
 * @param {Number} entityPixelSize The pixel size one entity has (entityPixelSize x entityPixelSize)
 * @param entityStateColors A list that mapping entity state with colors
 * @param {Immutable.List<Immutable.Map>} entities The entity list that will be drawn
 * @static
 */
function drawOnCanvas(canvas, entityPixelSize, entityStateColors, entities) {
  checkType(canvas, HTMLCanvasElement);
  checkType(entityPixelSize, Number);
  validateMapSchemaInList(entities, isAnEntity);

  clearCanvas(canvas);
  let drawOnGivenCanvas = curry(drawEntity)(canvas, entityPixelSize, entityStateColors);
  entities.forEach(drawOnGivenCanvas);
}

/**
 * Draws a single entity on a canvas
 * @param {HTMLCanvasElement} canvas The target canvas, where the entity will be drawn
 * @param {Number} entityPixelSize The pixel size the entity has (entityPixelSize x entityPixelSize)
 * @param entityStateColors A list that mapping entity state with colors
 * @param {Immutable.Map} entity The entity that will be drawn
 */
function drawEntity(canvas, entityPixelSize, entityStateColors, entity) {
  checkType(canvas, HTMLCanvasElement);
  checkType(entityPixelSize, Number);
  validateMapSchema(entity, isAnEntity);

  let ctx = canvas.getContext("2d");
  ctx.fillStyle = entityStateColors.get(entity.get("state"));
  ctx.fillRect(entity.get("x") * entityPixelSize, entity.get("y") * entityPixelSize, entityPixelSize, entityPixelSize);
}

/**
 * Clear the complete canvas
 * @param {HTMLCanvasElement} canvas The canvas which should be cleared
 * @returns {HTMLCanvasElement} canvas Cleared canvas
 */
function clearCanvas(canvas) {
  canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
  return canvas;
}

export default drawOnCanvas;

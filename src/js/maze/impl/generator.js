/**
 * Functions that generate the maze into the entities
 * @module maze/impl/generator
 */
import * as EntityFilter from '../entity/filter'
import {isAnEntity} from '../../schema/entity/entity'
import {validateMapSchemaInList, validateMapSchema} from '../../check-type/check-type'
import curry from 'lodash.curry'
import {EntityState} from '../../schema/entitystate/entitystate'

const filterStart = curry(EntityFilter.state)(EntityState.START);
const filterAir = curry(EntityFilter.state)(EntityState.AIR);
const filterWall = curry(EntityFilter.state)(EntityState.WALL);
const filterEnd = curry(EntityFilter.state)(EntityState.END);

/**
 * Secure replacement for native Math.random(), but it uses the cryptographically safe crypto API instead Math.random() pseudo
 * generator, which is not true random.
 * @returns {number} Random float number between 0 and 1
 * @see http://stackoverflow.com/questions/13694626/generating-random-numbers-0-to-1-with-crypto-generatevalues/13694869#13694869
 */
function random() {
  var arr = new Uint32Array(2);
  window.crypto.getRandomValues(arr);
  // keep all 32 bits of the the first, top 20 of the second for 52 random bits
  var mantissa = (arr[0] * Math.pow(2, 20)) + (arr[1] >>> 12);
  // shift all 52 bits to the right of the decimal point
  return mantissa * Math.pow(2, -52);
}

/**
 * Digs a path to a unvisited entity
 * @param mazeMap The maze list where the maze should be placed
 * @param entity Current entity which then is the start point for the next dig
 * @returns {Immutable.List<Immutable.List>} List with maze entities
 */
function traverseMazePath(mazeMap, entity) {
  validateMapSchemaInList(mazeMap, isAnEntity);
  validateMapSchema(entity, isAnEntity);
  let entityNumber = mazeMap.indexOf(entity);
  entity = entity.set("visited", true).set("way", true);

  let neighbors = mazeMap.filter(curry(EntityFilter.entity2FieldsAway)(entity)).filter(filterAir).filterNot(EntityFilter.visited);
  if (neighbors.isEmpty() && !(entity.get("state") === EntityState.START)) {
    const start = mazeMap.filter(filterStart).first();
    const distance = Math.abs(start.get("x") - entity.get("x")) + Math.abs(start.get("y") - entity.get("y"));
    if (mazeMap.some(filterEnd)) {
      let end = mazeMap.filter(filterEnd).first();
      if(distance > end.get("distance")) {
        //Mark new end
        entity = entity.set("state", EntityState.END).set("distance", distance);
        //Demark old end
        const endEntityNumber = mazeMap.indexOf(end);
        end = end.set("state", EntityState.AIR).delete("distance");
        mazeMap = mazeMap.set(endEntityNumber, end);
      }
    } else {
      // Mark first end
      entity = entity.set("state", EntityState.END).set("distance", distance);
    }
  }

  mazeMap = mazeMap.set(entityNumber, entity);
  neighbors.sortBy(() => random()).forEach((neighbor) => {
    // "Re"-find neighbor as it could be changed in the previous iteration
    let foundNeighbor = mazeMap.filter((entity) => (entity.get("x") === neighbor.get("x")) && (entity.get("y") === neighbor.get("y"))).first();
    if (!foundNeighbor.get("way")) {
      // SIDE-EFFECT BELOW
      let entityBetween = mazeMap.filter(curry(EntityFilter.between)(entity, foundNeighbor)).first();
      entityNumber = mazeMap.indexOf(entityBetween);
      entityBetween = entityBetween.set("visited", true).set("way", true);
      mazeMap = mazeMap.set(entityNumber, entityBetween);
      mazeMap = traverseMazePath(mazeMap, foundNeighbor);
    }
  });
  return mazeMap;
}

/**
 * Generate a maze in a list of entities
 * @param mazeMap Input entity list
 * @static
 * @returns {Immutable.List<Immutable.List>} List with maze entities
 */
function generateMaze(mazeMap) {
  let outSideWalls = mazeMap.filter(filterWall);
  var work = mazeMap.filterNot(filterWall).map((entity) => {
    return entity.set("visited", false).set("way", false)
  });
  work = traverseMazePath(work, work.filter(filterStart).first());
  return work.map((entity) => {
    return (entity.get("way")) ? entity : entity.set("state", EntityState.WALL)
  }).map((entity) => entity.delete("visited").delete("way").delete("distance")).concat(outSideWalls);
}

export {generateMaze};

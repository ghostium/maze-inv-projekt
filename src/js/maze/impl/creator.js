/**
 * Functions for maze creation
 * @module maze/impl/creator
 */

import {createEntity} from '../../schema/entity/entity'
import {Range} from 'immutable'
import {isAnEntity} from '../../schema/entity/entity'
import {checkType, validateMapSchemaInList} from '../../check-type/check-type';
import * as EntityFilter from '../entity/filter';
import curry from 'lodash.curry'
import {EntityState} from '../../schema/entitystate/entitystate'

/**
 * Create a empty maze list
 * @param {number} maxWidth The width of the maze (min. 6)
 * @param {number} maxHeight The height of the maze (min. 6)
 * @returns {Immutable.List<Immutable.Map>} A list of maze entities
 */
function createEmptyMaze(maxWidth, maxHeight) {
  checkType(maxHeight, Number);
  checkType(maxWidth, Number);

  maxWidth = (maxWidth < 6) ? 6 : maxWidth;
  maxHeight = (maxHeight < 6) ? 6 : maxHeight;

  return Range(0, maxWidth * maxHeight).map((number) => {
    let y = Math.floor(number / maxWidth);
    let x = number - (y * maxWidth);
    return createEntity(x, y);
  }).toList();
}

/**
 * Set all entities (1 width) around the maze to WALL
 * @param {number} maxWidth The width of the maze
 * @param {number} maxHeight he height of the maze
 * @param {Immutable.List<Immutable.Map>} mazeMap A list of maze entities
 * @returns {Immutable.List<Immutable.Map>} The maze list with the new border
 */
function createOuterBoarders(maxWidth, maxHeight, mazeMap) {
  checkType(maxHeight, Number);
  checkType(maxWidth, Number);
  validateMapSchemaInList(mazeMap, isAnEntity);

  maxWidth = (maxWidth < 6) ? 6 : maxWidth;
  maxHeight = (maxHeight < 6) ? 6 : maxHeight;

  return mazeMap.map((entity) => {
    //top and bottom
    if (entity.get("y") === 0 || entity.get("y") === maxHeight - 1) return entity.set("state", EntityState.WALL);
    //left and right
    if (entity.get("x") === 0 || entity.get("x") === maxWidth - 1) return entity.set("state", EntityState.WALL);
    return entity;
  });
}

/**
 * Set a random entity's state to START
 * @param {Immutable.List<Immutable.Map>} mazeMap A list of maze entities
 * @returns {Immutable.List<Immutable.Map>} The maze list with a start point
 */
function chooseStart(mazeMap) {
  validateMapSchemaInList(mazeMap, isAnEntity);
  let wantedEntity = mazeMap.filter(curry(EntityFilter.state)("AIR")).sortBy(() => Math.random()).first();
  return mazeMap.update(mazeMap.indexOf(wantedEntity), (entity) => entity.set("state", EntityState.START));
}


function replaceStartWithPlayer(mazeMap) {
  validateMapSchemaInList(mazeMap, isAnEntity);
  return mazeMap.map((entity) => (entity.get("state") === EntityState.START) ? entity.set("state", EntityState.PLAYER) : entity)
}

export {createEmptyMaze, createOuterBoarders, chooseStart, replaceStartWithPlayer}

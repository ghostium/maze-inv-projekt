/**
 * Various filter functions for entities
 * @module maze/entity/filter
 */
import {isAnEntity} from '../../schema/entity/entity'
import {List} from 'immutable'
import {validateMapSchema, checkType} from '../../check-type/check-type'

/**
 * Return true if entity object is on position x,y
 * @param {number} x expected x position of the entity
 * @param {number} y expected y position of the entity
 * @param entity the entity to check
 * @returns {boolean}
 */
function position(x, y, entity) {
  checkType(x, Number);
  checkType(y, Number);
  validateMapSchema(entity, isAnEntity);
  return entity.get("x") === x && entity.get("y") === y;
}

/**
* Return true if entity match state
* @param {string} state expected state
* @param entity the entity to check
* @returns {boolean}
*/
function state(state, entity) {
  checkType(state, String);
  validateMapSchema(entity, isAnEntity);
  return entity.get("state") === state;
}

/**
 * Return true if an entity has been visited by the maze generator
 * @param entity Entity that should be checked
 * @returns {boolean}
 */
function visited(entity) {
  validateMapSchema(entity, isAnEntity);
  return entity.get("visited");
}

/**
 * Return true if entity is between two other. No whitespace, the entity must directly connect horizontally or vertically
 * to the other ones.
 * @param firstEntity Start point
 * @param secondEntity End point
 * @param entityBetween The point that needs be directly connected to start & end
 * @returns {boolean}
 */
function between(firstEntity, secondEntity, entityBetween) {
  validateMapSchema(firstEntity, isAnEntity);
  validateMapSchema(secondEntity, isAnEntity);
  validateMapSchema(entityBetween, isAnEntity);

  function getPos(accessor) {
    if (firstEntity.get(accessor) === secondEntity.get(accessor)) {
      return firstEntity.get(accessor);
    } else if (firstEntity.get(accessor) < secondEntity.get(accessor)) {
      return firstEntity.get(accessor) + 1;
    } else {
      return firstEntity.get(accessor) - 1;
    }
  }

  return entityBetween.get("x") === getPos("x") && entityBetween.get("y") === getPos("y");
}

/**
 * Return true if the targetEntity is 2 fields in on direction away.
 * @param sourceEntity The source entity
 * @param targetEntity The entity which should be checked if it is 2 fields away
 * @returns {boolean}
 */
function entity2FieldsAway(sourceEntity, targetEntity) {
  validateMapSchema(sourceEntity, isAnEntity);
  validateMapSchema(targetEntity, isAnEntity);
  const offsets = List([[0, 2], [2, 0], [0, -2], [-2, 0]]);
  return offsets.some((offset) => position(sourceEntity.get("x") + offset[0], sourceEntity.get("y") + offset[1], targetEntity));
}

export {visited, state, position, between, entity2FieldsAway};

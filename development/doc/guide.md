# Start developing

1. Setup the environment
   Our env is node 7.3.0 with yarn 0.18.1

    ```bash
docker run -it -v `pwd`:/workspace --name=maze --net="host" --workdir=/workspace netczuk/node-yarn:node-7.3.0-wheezy-yarn-0.18.1 /bin/bash
    ```

2. Navigate to the folder /project and go to the next step.

3. Make some awesome changes.

4. Run build compile linting steps via our task runner. Our task runner
   is __do__. It's really simple to use. Just run

    ```bash
./do <taskname>
    ```

    Each task is just a collection of bash commands like a small
    embedded bash script. If you want read more about do go to the
    repository:
    [https://gitlab.com/ghostium/do](https://gitlab.com/ghostium/do)

5. Commit with the following conventions: [Commit conventions](commit.md)
   Also: [Why should I follow commit conventions?](why-commit-convention.md)


# Generating CHANGELOG.md
We use these three sections in changelog: **new features**, **bug fixes**, **breaking changes**.
This list could be generated by script when doing a release. Along with links to related commits.
Of course you can edit this change log before actual release, but it could generate the skeleton.


List of all subjects (first lines in commit message) since last release:

```bash
git log <last tag> HEAD --pretty=format:%s
```


When bisecting, you can ignore these by:
```bash
git bisect skip $(git rev-list --grep irrelevant <good place> HEAD)
```New features in this release
```bash
git log <last release> HEAD --grep feature
```
## Recognizing unimportant commits
These are formatting changes (adding/removing spaces/empty lines, indentation), missing semi colons, comments. So when you are looking for some change, you can ignore these commits - no logic change inside this commit.

When bisecting, you can ignore these by:
```bash
git bisect skip $(git rev-list --grep irrelevant <good place> HEAD)
```
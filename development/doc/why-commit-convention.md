# Why commit convention?
## Goals
- allow generating CHANGELOG.md by script
- allow ignoring commits by git bisect (not important commits like formatting
- provide better information when browsing the history

## Provide more information when browsing the history
This would add kinda “context” information.
Look at these messages (taken from last few angular’s commits):

- Fix small typo in docs widget (tutorial instructions)
- Fix test for scenario.Application - should remove old iframe
- docs - various doc fixes
- docs - stripping extra new lines
- Replaced double line break with single when text is fetched from Google
- Added support for properties in documentation

All of these messages try to specify where is the change. But they don’t share any convention...

Look at these messages:

- fix comment stripping
- fixing broken links
- Bit of refactoring
- Check whether links do exist and throw exception
- Fix sitemap include (to work on case sensitive linux)

Are you able to guess what’s inside ? These messages miss place specification...
So maybe something like parts of the code: **docs**, **docs-parser**, **compiler**, **scenario-runner**, …


I know, you can find this information by checking which files had been changed, but that’s slow. And when looking in git history I can see all of us tries to specify the place, only missing the convention.

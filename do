#!/usr/bin/env bash
VERSION="0.1.3";set -e;DEP_CHAIN=();DONE=();ARGS=($@);function _usage() { echo -e "DO Version ${VERSION}";echo -e "Usage: do <taskname>\n";echo -e "Available tasks:";tasks=$(< "${BASH_SOURCE[0]}" grep -Eo "function do_[a-zA-Z0]+" | cut -d "_" -f 2 | tr "0" ":");if [ -z "${tasks}" ];then echo "No tasks are defined";else echo -e "${tasks}";fi;};function _checkDuplicates() { tasks=$(< "${BASH_SOURCE[0]}" grep -Eo "function do_[a-zA-Z0]+" | cut -d "_" -f 2 | tr "0" ":");tasksasarray=($tasks);for task in "${tasksasarray[@]}";do count=0;for otherTask in "${tasksasarray[@]}";do if [ "$task" = "$otherTask" ];then count=$((count+1));fi;done;if [ "$count" -gt 1 ];then echo -e "\nError: Task ${task} is defined ${count} times. The last definition overwrites the other ones.\n";exit 1;fi;done;};function __handle_depchain_enter() { DEP_CHAIN+=($1);};function __handle_depchain_out() { if [[ ! ${#DEP_CHAIN[@]} -eq 0 ]];then unset DEP_CHAIN[${#DEP_CHAIN[@]}-1];fi;};function __fn_exists() { if [[ $(type -t "${1}" 2>/dev/null) == function ]];then return 1;else return 0;fi;};function __main() { _checkDuplicates;if [ ${#ARGS[@]} -lt 1 ];then _usage;exit 1;else dotask "${ARGS[0]}";fi;};function _do_task_definition_START() { :;};function _do_task_definition_END() { __main;};function dotask() { if [ -z "${1}" ];then log "Error: Passed empty function name";exit 1;fi;local function_name;function_name="do_""$(echo "${1}" | tr ':' '0')";if __fn_exists "${function_name}";then log "Error: Task '${1}' has no function!";exit 1;fi;if ! [[ ${DONE[*]} =~ ${1} ]];then DONE+=("${1}");log "Executing ${1}";__handle_depchain_enter "${1}";${function_name};__handle_depchain_out;else log "Executing ${1} - skipped, it already run.";fi;};function log() { local chain;chain=$(echo "${DEP_CHAIN[*]}" | tr " " "/");if [[ ! ${#DEP_CHAIN[@]} -eq 0 ]];then chain="${chain}/";fi;echo -e "DO/${chain}> ${1}";};
_do_task_definition_START ###############################
# Define tasks here

function do_build() {
dotask "build:webpack"
dotask "build:jsdoc"
}

function do_build0webpack() {
./node_modules/.bin/webpack
}

function do_build0jsdoc() {
./node_modules/.bin/jsdoc --configure .jsdoc.json --verbose
}

function do_clean() {
rm -rf dist/
rm -rf jsdoc/
}

function do_lint() {
log "Linting source"
./node_modules/.bin/eslint lib/**/*.js
}

function do_watch() {
log "Watching changes... -> Local server on :8080"
./node_modules/.bin/webpack-dev-server  --hot --inline --content-base dist/
}

_do_task_definition_END ###############################
